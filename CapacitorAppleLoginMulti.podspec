
  Pod::Spec.new do |s|
    s.name = 'CapacitorAppleLoginMulti'
    s.version = '0.0.1'
    s.summary = 'Multiplatform apple login for capacitor'
    s.license = 'MIT'
    s.homepage = 'https://gitlab.com/mutantantihero/capacitor-apple-login-multi'
    s.author = 'woozir'
    s.source = { :git => 'https://gitlab.com/mutantantihero/capacitor-apple-login-multi', :tag => s.version.to_s }
    s.source_files = 'ios/Plugin/**/*.{swift,h,m,c,cc,mm,cpp}'
    s.ios.deployment_target  = '11.0'
    s.dependency 'Capacitor'
  end