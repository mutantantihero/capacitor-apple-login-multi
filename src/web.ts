import { WebPlugin } from "@capacitor/core";
import { AppleLoginMultiPlugin } from "./definitions";

export class AppleLoginMultiWeb extends WebPlugin implements AppleLoginMultiPlugin {
  constructor() {
    super({
      name: "AppleLoginMulti",
      platforms: ["web"],
    });
  }

  async echo(options: { value: string }): Promise<{ value: string }> {
    console.log("ECHO", options);
    return options;
  }
}

const AppleLoginMulti = new AppleLoginMultiWeb();

export { AppleLoginMulti };

import { registerWebPlugin } from "@capacitor/core";
registerWebPlugin(AppleLoginMulti);
