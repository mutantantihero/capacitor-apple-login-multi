declare module "@capacitor/core" {
  interface PluginRegistry {
    AppleLoginMulti: AppleLoginMultiPlugin;
  }
}

export interface AppleLoginMultiPlugin {
  echo(options: { value: string }): Promise<{value: string}>;
}
